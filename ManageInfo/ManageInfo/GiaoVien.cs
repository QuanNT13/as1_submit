﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManageInfo
{
    public class GiaoVien : NguoiLaoDong
    {
        private double HeSoLuong {  get; set; }

        public GiaoVien()
        {
        }

        public GiaoVien(string hoTen, int namSinh, double luongCoBan, double heSoLuong)
        {
            HoTen = hoTen;
            NamSinh = namSinh;
            LuongCoBan = luongCoBan;
            HeSoLuong = heSoLuong;

        }

        public void NhapThongTin(double heSoLuong)
        {
            HeSoLuong = heSoLuong;
        }

        public new double TinhLuong()
        {
            return LuongCoBan * HeSoLuong * 1.25;
        }

        public new void XuatThongTin()
        {
            base.XuatThongTin();
            Console.WriteLine($"He so luong: {HeSoLuong}, Luong: {TinhLuong()}");
        }

        public void XuLy()
        {
            HeSoLuong += 0.6;
        }
    }
}

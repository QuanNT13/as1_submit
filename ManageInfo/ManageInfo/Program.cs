﻿using ManageInfo;

class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Nhap so luong giao vien: ");
        int n;

        while(!int.TryParse(Console.ReadLine(), out n) || n <= 0)
        {
            Console.WriteLine("--------");
            Console.WriteLine("Nhap so nguyen!!!");
            Console.WriteLine("Nhap so luong giao vien: ");
        }


        List<GiaoVien> danhSachGiaoVien = new List<GiaoVien>();

        for (int i = 0; i < n; i++)
        {
            Console.WriteLine($"Nhap thong tin cua giao vien {i + 1}: ");
            Console.WriteLine("Ho ten: ");
            string hoTen = Console.ReadLine();

            Console.WriteLine("Nam sinh: ");
            int namSinh;
            while(!int.TryParse(Console.ReadLine(),out namSinh) || namSinh < 1900 || namSinh > DateTime.Now.Year) 
            {
                Console.WriteLine("--------");
                Console.WriteLine("Nhap lai nam sinh cho giao vien !!!");
                Console.WriteLine("Nam sinh: ");
            }

            Console.WriteLine("Luong can ban: ");
            double luongCanBan;
            while(!double.TryParse(Console.ReadLine(),out luongCanBan))
            {
                Console.WriteLine("--------");
                Console.WriteLine("Nhap lai luong can ban !");
                Console.WriteLine("Luong can ban: ");
            }
            Console.WriteLine("He so luong: ");
            double heSoLuong;
            while(!double.TryParse(Console.ReadLine(), out heSoLuong))
            {
                Console.WriteLine("--------");
                Console.WriteLine("Nhap lai he so luong !");
                Console.WriteLine("He so luong: ");
            }
            Console.WriteLine("//////////////////////////////////");
            GiaoVien giaoVien = new GiaoVien(hoTen, namSinh, luongCanBan, heSoLuong);
            danhSachGiaoVien.Add(giaoVien);
        }

        Console.WriteLine("\nDanh sach giao vien: ");
        foreach (var item in danhSachGiaoVien)
        {
            item.XuatThongTin();
        }

        double luongThapNhat = danhSachGiaoVien[0].TinhLuong();
        GiaoVien giaoVienLuongThapNhat = danhSachGiaoVien[0];
        foreach (var item1 in danhSachGiaoVien)
        {
            if (item1.TinhLuong() < luongThapNhat)
            {
                luongThapNhat = item1.TinhLuong();
                giaoVienLuongThapNhat = item1;
            }
        }

        Console.WriteLine("\nThong tin giao vien co luong thap nhat: ");
        giaoVienLuongThapNhat.XuatThongTin();

        Console.ReadKey();
    }
}
